#
# ~/.bash_aliases
#

alias reset='tput reset'
alias clear='tput reset'
alias c='tput reset'
alias ls='ls -AF --color=auto'
alias l='ls -AFhl --color=auto'
alias rm='rm -v'
alias cp='cp -v'
alias mv='mv -v'
alias shred='shred -zxuvfn20'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias free='free -h'
alias nano='nano -l'
alias curl='curl -4'
alias sb='source ~/.bashrc'
alias diff='diff --color'
alias copy='xclip -selection c'
# alias xargs='xargs -t'
alias k='k9s --refresh 1'

git() {
  case $1 in
    commit)
      shift
      command git commit -S "$@"
      ;;
    *)
      command git "$@"
      ;;
  esac
}

cdr() {
  cd "$(git rev-parse --show-toplevel)"
}

calicoctl() {
	command calicoctl "$@" --allow-version-mismatch
}

tf-fmt() {
	echo terraform fmt -recursive .
	command terraform fmt -recursive .
}

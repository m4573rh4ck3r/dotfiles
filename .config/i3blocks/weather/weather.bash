#!/bin/bash

set -e

resp="$(curl --silent "https://api.weatherapi.com/v1/current.json?key=${weather_api_key?}&q=Cologne&aqi=no" | jq)"
condition=$(jq .current.condition.text <(echo ${resp}))
temp=$(jq .current.temp_c <(echo ${resp}))
echo -e " $temp °C $condition "

#!/bin/bash

BRIGHTNESS="$(xbacklight -get | cut -f1 -d'.')"

if [ $BRIGHTNESS -ge 80 ];
then
  echo "  ${BRIGHTNESS}% "
elif [ $BRIGHTNESS -ge 30 ];
then
  echo "  ${BRIGHTNESS}% "
else
  echo "  ${BRIGHTNESS}% "
fi

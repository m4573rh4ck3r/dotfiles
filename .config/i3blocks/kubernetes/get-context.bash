#!/bin/bash

set -e

current_context="$(kubectl config --kubeconfig ~/.kube/admin.conf current-context | cut -d'_' -f2,4)"

echo "${current_context}"

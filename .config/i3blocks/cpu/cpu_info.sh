#!/bin/sh
TEMP=$(sensors | grep 'Package id 0:\|Tdie' | grep ':[ ]*+[0-9]*.[0-9]*°C' -o | grep '+[0-9]*.[0-9]*°C' -o)
CPU_USAGE=$(mpstat 1 1 | awk '/Average:/ {printf("%s\n", $(NF-9))}')
CPU_FREQ=$(cat /proc/cpuinfo | grep MHz | cut -d ':' -f 2 | sort | sed 's/^ //g' | tail -n1)
echo "$CPU_FREQ $CPU_USAGE $TEMP" | awk '{ printf(" %s MHz %6s% @ %s \n"), $1, $2, $3 }'

#!/bin/bash

case $BLOCK_BUTTON in
	1)
		amixer cset numid=2,iface=MIXER,name='Capture Switch' toggle &>/dev/null
		;;
esac

case $(amixer cget numid=2,iface=MIXER,name='Capture Switch' | sed 1,2d | cut -d'=' -f2) in
	on)
		echo ""
		;;
	off)
		echo ""
		;;
esac

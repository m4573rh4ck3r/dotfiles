vim9script
# MAINTAINER: <M4573RH4CK3R m4573rh4ck3r@protonmail.com>
# GITLAB: https://gitlab.com/m4573rh4ck3r


# -------------------------------------------------- ESSENTIALS --------------------------------------------------

# don't use system defaults, start from scratch
g:skip_defaults_vim = 1

# don't behave like vi
set nocompatible

filetype plugin indent on
syntax on

set backspace=indent,eol,start

set encoding=utf-8

# determine term from $TERM environment variable
set term=$TERM

# show partial commands in the last line of the screen
set showcmd

# use system clipboard
set clipboard=unnamedplus

# don't wrap lines
set nowrap

# -------------------------------------------------- STATUSLINE --------------------------------------------------

set statusline=%<%f\ %-10.3n[%{strlen(&ft)?&ft:'none'}]%h%m%r%=%-14.(%l,%c%V%)

# shorten certain messages in the statusline
set shortmess=cfiloOtTx


# -------------------------------------------------- GUI --------------------------------------------------

# no GUI features
set guioptions-=r
set guioptions-=R
set guioptions-=l
set guioptions-=Lnd
set mouse=
set ttymouse=


# -------------------------------------------------- BELL --------------------------------------------------

# no bells
set novisualbell
set noerrorbells


# -------------------------------------------------- SEARCHING --------------------------------------------------

# while typing a search command, jump to the first matching pattern
set incsearch

# highlight search results
set hlsearch

# case sensitivity
set noignorecase


# -------------------------------------------------- OTHER --------------------------------------------------

# show matching brackets
set showmatch

set signcolumn=yes

# show a few lines of context around the cursor. Note that this makes the
# text scroll if you mouse-click near the start or end of the window.
set scrolloff=10

# we have a dark background
set background=dark

# automatically change working directory to the one of the currently opened file
# set autochdir

# trim White spaces before saving a buffer
def TrimWhitespace()
	var save = winsaveview()
	keeppatterns :%s/\s\+$//e
	call winrestview(save)
enddef

autocmd BufWritePre * :call TrimWhitespace()


# -------------------------------------------------- NETRW SETTINGS --------------------------------------------------

g:netrw_banner = 0
g:netrw_browse_split = 4
g:netrw_winsize = 15
g:netrw_altv = 1
g:netrw_hide = 0
g:netrw_liststyle = 3


# -------------------------------------------------- NERDTREE SETTINGS --------------------------------------------------

g:NERDTreeDirArrowExpandable = '▸'
g:NERDTreeDirArrowCollapsible = '▾'
g:NERDTreeShowHidden = 1
g:NERDTreeIgnore = ['^__pycache__$', '^node_modules$', '^.pytest_cache$']


# -------------------------------------------------- INDENTATION --------------------------------------------------

# do not recognize octal numbers for Ctrl-A and Ctrl-X, most users find it confusing.
set nrformats-=octal

set confirm

# auto-indentation for new lines
set smartindent

# make backspace remove entire tab
set smarttab

# don't convert tabs to spaces
set noexpandtab

# make a tab two spaces long
set tabstop=2
set softtabstop=2
set shiftwidth=2

# round indent to a multiple of tabstop
set shiftround
set smartindent

# automatically indent C code
set cindent

set copyindent


# -------------------------------------------------- VARIOUS --------------------------------------------------

# show relative numbers and the absolute number for the current line
set number relativenumber

# show the line and column number of the cursor position, separated by a comma
set ruler

# don't change mappings when changing the keyboard language
set nolangremap

# indicate a fast terminal connection
set ttyfast

# automatically read file changes from outside of vim
set autoread

# make sure we always have an empty line at the end of the file
set endofline
set fixeol

set cmdheight=2

set splitbelow splitright

# highlight the current line
set cursorline

# always show the status line
set laststatus=2


# -------------------------------------------------- CODE FOLDING --------------------------------------------------

# disable code folding
set nofoldenable

# do code folding manually
set foldmethod=manual

# set the maximum amount of nested fold levels
set foldnestmax=3


# -------------------------------------------------- SPELL CHECKING --------------------------------------------------

set spelllang=en,de

# when a word is CamelCased, assume "Cased" is a separate word
set spelloptions=camel

# enable spell checking
# set spell


# -------------------------------------------------- PERSISTENCE --------------------------------------------------

# don't create backup and swap files
set nobackup
set nowritebackup
set noswapfile

# automatically create undo and spell dirs if they don't exist
var HOME = $HOME
if !isdirectory($"{HOME}/.vim")
  mkdir($"{HOME}/.vim")
endif
if !isdirectory($"{HOME}/.vim/undo")
  mkdir($"{HOME}/.vim/undo")
endif
if !isdirectory($"{HOME}/.vim/spell")
  mkdir($"{HOME}/.vim/spell")
endif

# save undo history
set undofile
set undodir=~/.vim/undo

# When editing a file, always jump to the last known cursor position.
# Don't do it when the position is invalid, when inside an event handler
# (happens when dropping a file on gvim) and for a commit message (it's
# likely a different one than last time).
autocmd BufReadPost *
	\ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
	\ |   exe "normal! g`\""
	\ | endif


# -------------------------------------------------- COMPLETION --------------------------------------------------

set path+=**
set wildmenu
set wildoptions=pum
set wildmode=longest:list,longest

# don't look for possible completions in files matching these patterns
set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/node_modules/*,*/.terraform/*
set wildignore+=*.DS_STORE,*.db,node_modules/**,*.jpg,*.png,*.gif,.terraform/**
set wildignore+=*/coverage

# completion function
# set omnifunc=syntaxcomplete#Complete

set completeopt=menu,preview,noselect

# -------------------------------------------------- MAPPINGS --------------------------------------------------

# set leader key
nnoremap <Space> <Nop>
var mapleader = " "
g:mapleader = " "

# convenient mappings
nnoremap <Leader>s :source ~/.vimrc<CR>:nohlsearch<CR>:echo<CR>
nnoremap <Leader>w :w<CR>
nnoremap <Leader>q :q<CR>
nnoremap <Leader>x :x<CR>

# toggle spell checking
noremap <F5> :set spell!<CR>
noremap <F6> :set spelllang=en<CR>
noremap <F7> :set spelllang=de<CR>

# disable highlight when <Backspace> is pressed
nnoremap <silent> <BS> :nohlsearch<CR>:echo<CR>

# insert newline above and below and start insert mode
nnoremap <Leader>o O<ESC>o

# toggle nerdtree
nnoremap <Leader>f :NERDTreeToggle<CR>
nnoremap <Leader>b :NERDTreeFromBookmark<Space>

# jump to the directory of the currently open file in nerdtree
nnoremap <Leader>jj :NERDTreeFind<CR>

# prevent usage of buffer
nnoremap <Leader>d "_d
nnoremap <Leader>c "_c
nnoremap <Leader>D "_D
nnoremap <Leader>C "_C
nnoremap <Leader>S "_S
nnoremap <Leader>X "_X

# remove windows line breaks
nnoremap <Leader>m :e<Space>++ff=dos<CR>:e<Space>++ff=unix<CR>:%s/\r//g<CR>

# yank until end of line
nnoremap <S-y> y$

# move between windows
nnoremap <C-j> <C-W><C-j>
nnoremap <C-k> <C-W><C-k>
nnoremap <C-l> <C-W><C-l>
nnoremap <C-h> <C-W><C-h>

tnoremap <C-j> <C-w>j
tnoremap <C-k> <C-w>k
tnoremap <C-l> <C-w>l
tnoremap <C-h> <C-w>h

# open a terminal in new split
nnoremap <silent> <leader><Enter> :terminal<CR>
nnoremap <silent> <leader>tt :tabnew<CR>:term ++curwin<CR>

# resize window
nnoremap <Up> :resize +2<CR>
nnoremap <Down> :resize -2<CR>
nnoremap <Left> :vertical resize +2<CR>
nnoremap <Right> :vertical resize -2<CR>

# # buffers
# nnoremap <Leader>b :b<Space>

# open buffer by number
nnoremap <Leader>1 :1b<CR>
nnoremap <Leader>2 :2b<CR>
nnoremap <Leader>3 :3b<CR>
nnoremap <Leader>4 :4b<CR>
nnoremap <Leader>5 :5b<CR>
nnoremap <Leader>6 :6b<CR>
nnoremap <Leader>7 :7b<CR>
nnoremap <Leader>8 :8b<CR>
nnoremap <Leader>9 :9b<CR>
nnoremap <Leader>0 :10b<CR>

# tabs
nnoremap <Leader>tn :tabnew<CR>
nnoremap <Leader>tc :tabclose<CR>
nnoremap <Leader>to :tabonly<CR>

# switch between tabs
nmap <leader>t1 1gt
nmap <leader>t2 2gt
nmap <leader>t3 3gt
nmap <leader>t4 4gt
nmap <leader>t5 5gt
nmap <leader>t6 6gt
nmap <leader>t7 7gt
nmap <leader>t8 8gt
nmap <leader>t9 9gt

# create split with empty buffer
nnoremap <Leader>hn :leftabove  vnew<CR>
nnoremap <Leader>ln :rightbelow vnew<CR>
nnoremap <Leader>kn :leftabove  new<CR>
nnoremap <Leader>jn :rightbelow new<CR>

nnoremap <Leader>h :NERDTreeToggle<CR><C-w>H:NERDTreeToggle<CR>
nnoremap <Leader>j :NERDTreeToggle<CR><C-w>J:NERDTreeToggle<CR>
nnoremap <Leader>k :NERDTreeToggle<CR><C-w>K:NERDTreeToggle<CR>
nnoremap <Leader>l :NERDTreeToggle<CR><C-w>L:NERDTreeToggle<CR>

# fix spelling
imap <c-l> <c-g>u<Esc>[s1z=`]a<c-g>u

# open new empty buffer in vertical split
nnoremap <Leader>vn :vsplit enew<CR>

# open new empty buffer in horizontal split
nnoremap <Leader>hn :split enew<CR>

# read current file from disk
nnoremap <Leader>r :checktime<CR>:NERDTreeRefreshRoot<CR>

# complete file names
inoremap <C-f> <C-x><C-f>

vnoremap <Leader>s ::!sort<CR>
vnoremap <Leader>u ::!uniq<CR>


# -------------------------------------------------- COC.NVIM --------------------------------------------------

set updatetime=300

def CheckBackspace(): bool
  var col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
enddef

# use tab for trigger completion
inoremap <silent><expr> <TAB>
	\ coc#pum#visible() ? coc#pum#next(1) :
	\ CheckBackspace() ? "\<Tab>" :
	\ coc#refresh()

# use Shift-tab for trigger completion backwards
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

# complete currently selected item
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#_select_confirm()
                              \ : "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

autocmd FileType terraform,tf b:coc_root_patterns = ["~/.terraform", "~/.terraform.d", "terraform/*/.terraform", ".terraform", "~/.terraform.d/plugin-cache"]

# Use gn and gN to navigate diagnostics
# Use `:CocDiagnostics` to get all diagnostics of current buffer in location list
nnoremap <silent> gn <Plug>(coc-diagnostic-prev)
nnoremap <silent> gN <Plug>(coc-diagnostic-next)

# GoTo code navigation
nnoremap <silent> gd <Plug>(coc-definition)
nnoremap <silent> gy <Plug>(coc-type-definition)
nnoremap <silent> gi <Plug>(coc-implementation)
nnoremap <silent> gr <Plug>(coc-references)


# -------------------------------------------------- VIM-GO --------------------------------------------------

g:go_imports_autosave = 0


# -------------------------------------------------- ABBREVIATIONS --------------------------------------------------

# abbreviations
ab nnn M4573RH4CK3R
ab eee m4573rh4ck3r@protonmail.com
ab ccc* **************************************************
ab ccc- --------------------------------------------------
ab ccc# ##################################################
ab ccc" """"""""""""""""""""""""""""""""""""""""""""""""""


# -------------------------------------------------- COLORS --------------------------------------------------

# You might have to force true color when using regular vim inside tmux as the
# colorscheme can appear to be grayscale with "termguicolors" option enabled.
# if !has('gui_running') && &term =~ '^\%(screen\|tmux\)'
  # &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  # &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
# endif
set termguicolors
highlight Pmenu ctermbg=red guibg=red ctermfg=white guifg=white


# -------------------------------------------------- TERRAFORM-LS --------------------------------------------------

g:terraform_fmt_on_save = 1


defcompile

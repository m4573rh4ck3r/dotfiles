" MAINTAINER: <M4573RH4CK3R m4573rh4ck3r@protonmail.com>
" GITLAB: https://gitlab.com/m4573rh4ck3r

" don't behave like vi
set nocompatible

let skip_defaults_vim=1

" StatusLine configuration
set statusline=%<%f\ %h%m%r%=%-14.(%l,%c%V%)\ %P

" Allow backspacing over everything in insert mode.
set backspace=indent,eol,start

set showcmd " display incomplete commands

" Show @@@ in the last line if it is truncated.
set display=truncate

" Show a few lines of context around the cursor.  Note that this makes the
" text scroll if you mouse-click near the start or end of the window.
set scrolloff=7

set incsearch " while typing a search command, show matching pattern highlighted

" Do not recognize octal numbers for Ctrl-A and Ctrl-X, most users find it
" confusing.
set nrformats-=octal

" Set leader key
nnoremap <Space> <Nop>
let mapleader = " "

" Don't use Ex mode, use Q for formatting.
" Revert with ":unmap Q".
map Q gq

" Fast save and quit
nnoremap <Leader>w :w<CR>
nnoremap <Leader>q :q<CR>

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
" Revert with ":iunmap <C-U>".
inoremap <C-U> <C-G>u<C-U>

" Quick reload of vimrc
nmap <Leader>s :source $MYVIMRC<CR>

" Switch syntax highlighting on when the terminal has colors or when using the
" GUI (which always has colors).
if &t_Co > 2 || has("gui_running")
  syntax on

  " Hhighlighting strings inside C comments.
  " Revert with ":unlet c_comment_strings".
  let c_comment_strings=1
endif

" Only do this part when Vim was compiled with the +eval feature.
if 1

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  " Revert with ":filetype off".
  filetype plugin indent on

  " Put these in an autocmd group, so that you can revert them with:
  " ":augroup vimStartup | au! | augroup END"
  augroup vimStartup
    au!

    " When editing a file, always jump to the last known cursor position.
    " Don't do it when the position is invalid, when inside an event handler
    " (happens when dropping a file on gvim) and for a commit message (it's
    " likely a different one than last time).
    autocmd BufReadPost *
      \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
      \ |   exe "normal! g`\""
      \ | endif

  augroup END

endif

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
" Revert with: ":delcommand DiffOrig".
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif

if has('langmap') && exists('+langremap')
  " Prevent that the langmap option applies to characters that result from a
  " mapping.  If set (default), this may break plugins (but it's backward
  " compatible).
  set nolangremap
endif

" Search highlighting
set hlsearch

" Speeeeeed
set ttyfast
set lazyredraw

" wait for following key without timeout
set notimeout

" make p in Visual mode replace the selected text with the yank register
vnoremap p <Esc>:let current_reg = @"<CR>gvdi<C-R>=current_reg<CR><Esc>

" Conflict markers
" highlight conflict markers
match ErrorMsg '\v^[<\|=|>]{7}([^=].+)?$'

" shortcut to jump to next conflict marker
nnoremap <silent> <leader>c /\v^[<\|=>]{7}([^=].+)?$<CR>

" <Leader>R = Converts tabs to spaces in document
nmap <Leader>R :retab<CR>

" Disable highlight when <Backspace> is pressed
nnoremap <silent> <BS> :nohlsearch<CR>

set title

set autoread " automatically read file changes from outside of vim

set endofline

set number

set relativenumber

set ruler

set showmatch

set undofile

set history=1000

set undolevels=1000

set noignorecase

set smartcase

set confirm

set smarttab

set smartindent

set autoindent

set shiftwidth=3

set softtabstop=3

set tabstop=3

set cindent

set noerrorbells

set visualbell

set splitbelow splitright

set cursorline

set laststatus=2

set wildmenu
set wildchar=<TAB>
set wildmode=list:longest
set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/node_modules/*,*/.terraform/*
set wildignore+=*.DS_STORE,*.db,node_modules/**,*.jpg,*.png,*.gif,.terraform/**
set wildignore+=*/coverage

" Code folding
set foldmethod=manual
set foldnestmax=3
set nofoldenable

set copyindent

" Use system clipboard
set clipboard=unnamedplus

" Rize window
nnoremap <Up> :resize +2<CR>
nnoremap <Down> :resize -2<CR>
nnoremap <Left> :vertical resize +2<CR>
nnoremap <Right> :vertical resize -2<CR>

set updatetime=3000

set path+=**

set term=$TERM

" Enable 256 colors palette in Gnome Terminal
if $COLORTERM == 'gnome-terminal'
    set t_Co=256
endif

set background=dark

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" MULTIPURPOSE TAB KEY
" Indent if we're at the beginning of a line. Else, do completion.
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! InsertTabWrapper()
    let col = col('.') - 1
    if !col || getline('.')[col - 1] !~ '\k'
        return "\<tab>"
    else
        return "\<c-p>"
    endif
endfunction

inoremap <expr> <tab> InsertTabWrapper()
inoremap <s-tab> <c-n>

" Only autoclose double quotes if there is no keymap character right to the cursor and if we are not at a newline
function! AutocloseDoubleQuotes()
	let col = col('.') - 1
	if getline('.')[col] == "\""
		return "\<Right>"
	elseif getline('.')[col] !~ '\k' && col
		return "\"\"\<Left>"
	else
		return "\""
	endif
endfunction

inoremap <expr> " AutocloseDoubleQuotes()

" Only autoclose single quotes if there is no keymap character right to the curor
function! AutocloseSingleQuotes()
	let col = col('.') - 1
	if getline('.')[col] == "\'"
		return "\<Right>"
	elseif getline('.')[col] !~ '\k'
		return "\'\'\<Left>"
	else
		return "\'"
	endif
endfunction

inoremap <expr> ' AutocloseSingleQuotes()

" Only autoclose single quotes if there is no keymap character right to the curor
function! AutocloseBrackets()
	let col = col('.') - 1
	if getline('.')[col] !~ '\k'
		return "\(\)\<Left>"
	else
		return "\("
	endif
endfunction

inoremap <expr> ( AutocloseBrackets()

function! PreventDoubleClosingBrackets()
	let col = col('.') - 1
	if getline('.')[col] == ')'
		return "\<Right>"
	else
		return "\)"
	endif
endfunction

inoremap <expr> ) PreventDoubleClosingBrackets()


" Only autoclose single quotes if there is no keymap character right to the curor
function! AutocloseCurlyBrackets()
	let col = col('.') - 1
	if getline('.')[col] !~ '\k'
		return "\{\}\<Left>"
	else
		return "\{"
	endif
endfunction

inoremap <expr> { AutocloseCurlyBrackets()

function! PreventDoubleClosingCurlyBrackets()
	let col = col('.') - 1
	if getline('.')[col] == '}'
		return "\<Right>"
	else
		return "\}"
	endif
endfunction

inoremap <expr> } PreventDoubleClosingCurlyBrackets()

" Only autoclose single quotes if there is no keymap character right to the curor
function! AutocloseSquareBrackets()
	let col = col('.') - 1
	if getline('.')[col] !~ '\k'
		return "\[\]\<Left>"
	else
		return "\["
	endif
endfunction

inoremap <expr> [ AutocloseSquareBrackets()

function! PreventDoubleClosingSquareBrackets()
	let col = col('.') - 1
	if getline('.')[col] == ']'
		return "\<Right>"
	else
		return "\]"
	endif
endfunction

inoremap <expr> ] PreventDoubleClosingSquareBrackets()

" Disable scrollbars (real hackers don't use scrollbars for navigation!)
set guioptions-=r
set guioptions-=R
set guioptions-=l
set guioptions-=Lnd
set mouse=
set ttymouse=

" Remove the Windows ^M - when the encodings gets messed up
noremap <Leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'met ttymouse=

" Function to trim whitespaces on file save
fun! TrimWhitespace()
	let l:save = winsaveview()
	keeppatterns %s/\s\+$//e
	call winrestview(l:save)
endfun

" Trim Whitespaces before saving a buffer
autocmd BufWritePre * :call TrimWhitespace()

" Spell checking
set nospell " disable spell checking by default
set spelllang=en_us,de_de
" toggle spell checking
map <F5> <ESC>:set spell!<CR>
" shortcuts to change spell checking language
map <F6> <ESC>:set spelllang=en_us<CR>
map <F7> <ESC>:set spelllang=de_de<CR>

" Vim only updates the current mode after pressing ESC and any other key?!
inoremap <ESC> <ESC><ESC>

" Shortcut for deleting without cutting text
nnoremap <Leader>d "_d

" Abbreviations
ab nnn M4573RH4CK3R
ab eee m4573rh4ck3r@protonmail.com

let g:terraform_align=1
let g:terraform_fmt_on_save=1

let s:clip = 'clip.exe'  " change this path according to your mount point
if executable(s:clip)
  augroup WSLYank
    autocmd!
    autocmd TextYankPost * if v:event.operator ==# 'y' | call system(s:clip, @0) | endif
  augroup END
endif

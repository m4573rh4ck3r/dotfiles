#!/bin/bash

set -e

while IFS= read -r line;
do
  res=$(echo $line | cut -d' ' -f4 | cut -d'+' -f1 | sed 's/x/**2+/g' | sed 's/$/**2/g')
  output=$(echo $line | cut -d' ' -f1)

  set -x

  dpi=$(python -c "import math; print(int(math.sqrt(${res})/15.6))")
  xrandr --output "${output}" --dpi "${dpi}"

  set +x
done <<< $(xrandr -q | grep " connected")

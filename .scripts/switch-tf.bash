#!/bin/bash

set -ex

cd ~/go/src/github.com/hashicorp/terraform
git checkout main
git pull
git checkout "v${1?}"
go build -v -o ~/go/bin/terraform

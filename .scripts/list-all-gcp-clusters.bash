#!/bin/bash

# set -e

projects=($(for project in $(gcloud projects list 2>/dev/null | awk '{print $1}' | sed 1d | grep -vE "^ip-" | tr '\n' ' '); do echo -n "$project "; done))
for project in ${projects[@]};
do
	gcloud container clusters list --project=$project --region=europe-west1 2>/dev/null | sed 1d
done

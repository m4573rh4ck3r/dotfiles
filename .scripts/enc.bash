#!/bin/bash

set -eux

gpg --encrypt --sign --armor -r "$1" "$2"

#!/bin/bash

set -eo pipefail

vim --version | grep -vE "Compilation|Linking" | grep -v "rc file" | head -n-3 | sed 1,2d | sed 1,2d | tr ' ' '\n' | sed '/^$/d' | sed '/^\+/d' | sort

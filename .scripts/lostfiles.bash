#!/bin/bash

set -e

find /etc /usr /opt | LC_ALL=C pacman -Qqo - 2>&1 >&- >/dev/null | cut -d ' ' -f 5- | grep -v "/usr/share/man" | grep -v "ca-certificates" | grep -v "/usr/share/mime" | grep -v "/usr/share/vim" | grep -v "/etc/ssl/certs"

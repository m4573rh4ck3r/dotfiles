#!/bin/bash

set -eux

gpg --decrypt -o "$2" "$1"

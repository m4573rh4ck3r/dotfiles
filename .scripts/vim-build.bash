#!/bin/bash

set -exo pipefail

make clean
make distclean
git clean -xdf

git checkout master
git pull
git checkout $(git tag | sort -n | tail -n1)

./configure \
  --disable-arabic \
	--disable-darwin \
	--disable-selinux \
  --disable-netbeans \
  --disable-rightleft \
  --disable-gui \
  --enable-fail-if-missing \
  --enable-terminal \
	--enable-gpm=no \
  --prefix=/usr \
  --with-features=huge \
	--with-x

make -j12

#!/bin/bash

set -ex

reflector --verbose --latest 200 --number 5 --sort rate --save /etc/pacman.d/mirrorlist

#!/usr/bin/env bash

set -e

xrandr --dpi 290

xinput set-prop "DELL097D:00 04F3:311C Touchpad" "libinput Tapping Enabled" 1

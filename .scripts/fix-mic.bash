#!/bin/bash

set -e

amixer cset numid=2,iface=MIXER,name='Capture Switch' off
amixer cset numid=2,iface=MIXER,name='Capture Switch' on
amixer cset numid=1,iface=MIXER,name='Capture Volume' 50%
amixer cset numid=1,iface=MIXER,name='Capture Volume' 100%

SHELL := /bin/bash
pacman_packages := $(shell cat pacman_packages.txt | tr '\n' ' ' | sed 's, $$,,g')
etc_files := $(shell find root/etc -type f)
i3_files := $(shell find .config/i3 -type f)
i3blocks_files := $(shell find .config/i3blocks -type f)
vim_files := $(shell find .vim -type f) .vimrc
vim_plugins := "github.com/neoclide/coc.nvim" "github.com/preservim/nerdtree" "github.com/tpope/vim-commentary" "github.com/tpope/vim-surround" "github.com/fatih/vim-go" "github.com/hashivim/vim-terraform" "github.com/towolf/vim-helm"

ifeq ($(distdir),)
distdir := _dist
endif

ifeq ($(prefix),)
prefix := "/"
endif

ifeq ($(vim_prefix),)
vim_prefix := $(HOME)
endif

ifeq ($(vim_plugins_prefix),)
vim_plugins_prefix := $(vim_prefix)/.vim/pack/plugins/start
endif

ifeq ($(i3_prefix),)
i3_prefix := $(HOME)/.config
endif

ifeq ($(i3blocks_prefix),)
i3blocks_prefix := $(HOME)/.config
endif

ifneq ($(SUDO_USER),)
user:=$(SUDO_USER)
else
user:=$(USER)
endif

.PHONY: all
all: build

test:
	@echo vim-prefix: $(vim_prefix)
	@echo vim-plugins-prefix: $(vim_plugins_prefix)

$(distdir):
	@install -d -m 0755 -o $(shell id -u $(user)) -g $(shell id -g $(user)) $(distdir)

.PHONY: build
build: $(distdir)
	@$(foreach file,$(etc_files), install $(shell stat -c '-m %a' $(file)) -D $(file) $(distdir)/$(file);)
	@$(foreach file,$(vim_files), install $(shell stat -c '-m %a' $(file)) -o $(shell id -u $(user)) -g $(shell id -g $(user)) -D $(file) $(distdir)/$(file);)
	@$(foreach file,$(i3_files), install $(shell stat -c '-m %a' $(file)) -o $(shell id -u $(user)) -g $(shell id -g $(user)) -D $(file) $(distdir)/$(file);)
	@$(foreach file,$(i3blocks_files), install $(shell stat -c '-m %a' $(file)) -o $(shell id -u $(user)) -g $(shell id -g $(user)) -D $(file) $(distdir)/$(file);)

.PHONY: install-vim-plugins
install-vim-plugins:
	@$(foreach plugin,$(vim_plugins), \
		if [ -d $(vim_plugins_prefix)/$(shell echo $(plugin) | rev | cut -d'/' -f1 | rev) ]; then cd $(vim_plugins_prefix)/$(shell echo $(plugin) | rev | cut -d'/' -f1 | rev) && git pull; \
		else git clone https://$(plugin).git $(vim_plugins_prefix)/$(shell echo $(plugin) | rev | cut -d'/' -f1 | rev); fi \
	;)
	@cd $(vim_plugins_prefix)/coc.nvim && yarn

.PHONY: clean
clean:
	@rm -rvf $(distdir)

.PHONY: install
install: install-pacman-packages
	@$(foreach file,$(etc_files), sudo install $(shell stat -c '-m %a' $(file)) -o 0 -g 0 -D $(distdir)/$(file) $(prefix)$(file);)
	@$(foreach file,$(vim_files), install $(shell stat -c '-m %a' $(file)) -o 0 -g 0 -D $(distdir)/$(file) $(vim_prefix)/$(file);)
	@$(foreach file,$(i3_files), install $(shell stat -c '-m %a' $(file)) -o 0 -g 0 -D $(distdir)/$(file) $(i3_prefix)/$(file);)
	@$(foreach file,$(i3blocks_files), install $(shell stat -c '-m %a' $(file)) -o 0 -g 0 -D $(distdir)/$(file) $(i3blocks_prefix)/$(file);)

.PHONY: install-pacman-packages
install-pacman-packages:
	@pacman -Syy $(pacman_packages)

.PHONY: uninstall
uninstall: uninstall-pacman-packages

.PHONY: uninstall-pacman-packages
uninstall-pacman-packages:
	@pacman -Rsun $(pacman_packages)

uninstall-vim-plugins:
	@$(foreach vim_plugin,$(vim_plugins), rm -rvf $(vim_plugins_prefix)/$(shell echo $(plugin) | rev | cut -d'/' -f1 | rev);)

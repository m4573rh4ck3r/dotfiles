#
# ~/.bashrc
#

[[ $- != *i* ]] && return

[ $DISPLAY ] && shopt -s checkwinsize

# define environment variables

GPG_TTY=$(tty)
PS1="[\u@\h \$? \w]\$ "
GOPATH="$HOME/go"
GOBIN="$GOPATH/bin"
GO111MODULE=on
PATH="$GOBIN:$HOME/.local/bin:$HOME/.bin:$HOME/.cargo/bin:$PATH"
KUBECONFIG="$HOME/.kube/admin.conf"
EDITOR=vim
TERMINAL=alacritty
CLOUDSDK_PYTHON=/usr/bin/python3
USE_GKE_GCLOUD_AUTH_PLUGIN=True
TF_PLUGIN_CACHE_DIR="${HOME}/.terraform"

export CLOUDSDK_PYTHON EDITOR GO111MODULE GOBIN GOPATH GPG_TTY KUBECONFIG PATH PS1 TERMINAL TF_PLUGIN_CACHE_DIR USE_GKE_GCLOUD_AUTH_PLUGIN

# SSH configuration

export SSH_AUTH_SOCK=/run/user/1000/gcr/ssh
export SSH_AGENT_PID="$(pgrep -fa ssh-agent | grep /run/user/1000/keyring/.ssh | awk '{print $1}' 2>/dev/null)"
dbus-update-activation-environment --all

# source additional files

[ -f "$HOME/.bash_aliases" ] && source "$HOME/.bash_aliases" || true
[ -f "$HOME/.bash_extras" ] && source "$HOME/.bash_extras" || true
[ -f "$HOME/.bash_completions" ] && source "$HOME/.bash_completions" || true

# setup bash HIST control

export HISTCONTROL="ignorespace:ignoredups:erasedups"
export HISTIGNORE="clear:reset"
export HISTSIZE=10000
export HISTFILESIZE=10000

# when starting a vim terminal, go to the git root directory if in a repository

[ -n "${VIM_TERMINAL}" ] && git -C . rev-parse 2>/dev/null && cd $(git rev-parse --show-toplevel) || true

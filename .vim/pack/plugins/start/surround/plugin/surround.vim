" delete surrounding characters
nnoremap ds" F"xf"x
nnoremap ds' F'xf'x
nnoremap ds( F(xf)x
nnoremap ds{ F{xf}x
nnoremap ds[ F[xf]x
nnoremap ds< F<xf>x

" add surrounding characters
nnoremap gs" <S-b>i"<ESC><S-e>a"<ESC>
nnoremap gs' <S-b>i'<ESC><S-e>a'<ESC>
nnoremap gs( <S-b>i(<ESC><S-e>a)<ESC>
nnoremap gs{ <S-b>i{<ESC><S-e>a}<ESC>
nnoremap gs[ <S-b>i[<ESC><S-e>a]<ESC>
nnoremap gs< <S-b>i<<ESC><S-e>a><ESC>

nnoremap gsw" ciw""<ESC>hp
nnoremap gsw' ciw''<ESC>hp
nnoremap gsw( ciw()<ESC>hp
nnoremap gsw{ ciw{}<ESC>hp
nnoremap gsw[ ciw[]<ESC>hp
nnoremap gsw< ciw<><ESC>hp

vnoremap gs" s""<ESC><S-p>
vnoremap gs' s''<ESC><S-p>
vnoremap gs( s()<ESC><S-p>
vnoremap gs{ s{}<ESC><S-p>
vnoremap gs[ s[]<ESC><S-p>

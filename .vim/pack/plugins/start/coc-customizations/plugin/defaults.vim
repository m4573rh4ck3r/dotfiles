vim9script

# Use tab for trigger completion with characters ahead and navigate.
# NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
# other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>Check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

# " Make <CR> auto-select the first completion item and notify coc.nvim to
# " format on enter, <cr> could be remapped by other vim plugin
# inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
# 	\: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
# " use <tab> for trigger completion and navigate to the next complete item
def Check_back_space(): bool
	var col = col('.') - 1
	return !col || getline('.')[col - 1]  =~ '\s'
enddef

inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm() : "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

# GoTo code navigation.
nnoremap <silent> gd <Plug>(coc-definition)
nnoremap <silent> gy <Plug>(coc-type-definition)
nnoremap <silent> gi <Plug>(coc-implementation)
nnoremap <silent> gr <Plug>(coc-references)
